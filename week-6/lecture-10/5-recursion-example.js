function sum(number) {
	console.log(number);
	if (number === 0)
		return number;
	else
		return number + sum(number - 1);
}
// sum(5)
// 5 + sum(4)
// 5 + 4 + sum(3)
// 5 + 4 + 3 + sum(2)
// 5 + 4 + 3 + 2 + sum(1)
// 5 + 4 + 3 + 2 + 1 + sum(0)
// 5 + 4 + 3 + 2 + 1 + 0
// 15

console.log("sum is:", sum(5)); // prints 15 (= 5 + 4 + 3 + 2 + 1)