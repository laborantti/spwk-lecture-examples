const arr = [23, 234234, 123, 132, NaN, 123124];
const sum = arr.reduce(
	(acc, value) => {
		if (isNaN(value))
			value = 0;
		return acc + value;
})
console.log(sum);