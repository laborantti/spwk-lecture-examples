function myCalculator(num1, num2, myCallback) {
    let sum = num1 + num2;
    myCallback(sum);
}

function myDisplayer(numb) {
    console.log("Number:", numb);
}

myCalculator(5, 5, myDisplayer);

myCalculator(2, 4, function(numb) {
	console.log("The number is: ", numb);
})

const arr = [234, 2342, 234234, 23423, 12];
arr.forEach((value) => {
	console.log("the number is", value);
})