const ages = [13, 21, 33, null, 17, undefined, 46];
const sumOfAges = ages
	.map(value => {
		if (value === null || value === undefined)
			return 0;
		else
			return value;
	})
	.reduce((acc, value, i, arr) => {
	console.log(value, i);
	console.log(ages);
	console.log(arr);
	return acc + value;
}, 5);
console.log(sumOfAges); // prints 130 (= 13 + 21 + 33 + 17 + 46)


