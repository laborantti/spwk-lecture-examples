const myArray = [ -23, 1243, -123, 123123 ]

const printStats = function(value, index, array) {
    console.log(`${index}: ${value}`);
}

myArray.forEach(printStats);

forEach(myArray, printStats);

function forEach(arr, func) {
	for (let i = 0; i < arr.length; i++)
	{
		func(arr[i], i, arr); // (value, index, array)
	}
}