const student = {
	name: "Aili",
	credits: 45,
	courses: [
		{
			name: "JavaScript Basics",
			grade: 3,
		},
		{
			name: "Intro to Programming",
			grade: 2,
		},
		{
			name: "Functional Programming",
			grade: 5,
		},
	],
};

// console.log(student.courses);

// Aili got 3 from Intro to Programming

/* longer version of find:
const course = student.courses.find((element) => {
	if (element.name === "JavaScript Basics") {
		return true;
	} else {
		return false;
	}
});
*/

const course = student.courses.find((element) => element.name === "JavaScript Basics");
console.log(`${student.name} got ${course.grade} from ${course.name}`);

function addCourse(courseName, courseGrade) {
	const course = {
		name: courseName,
		grade: courseGrade,
	};
	student.courses.push(course);
}

addCourse("Nakkiveneilyn perusteet", 5);
addCourse("Terrestristen planeettojen basalttinen vulkanismi", 5);
console.log(student.courses);