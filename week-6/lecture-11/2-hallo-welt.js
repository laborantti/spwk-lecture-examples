const helloLookupObject = {
	"en": "Hello World",
	"fi": "Hei maailma",
	"de": "Hallo Welt",
	"se": "Hejsan Världen",
};

function printHello(language) {
	if (language in helloLookupObject) {
		console.log(helloLookupObject[language]);
	} else {
		console.log("Language not supported! Supported languages: "
		 + Object.keys(helloLookupObject).join(", "));
	}
}

printHello("se");
printHello("en");
printHello("es");