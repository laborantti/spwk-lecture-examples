const student = {
	name: "Aili",
	credits: 45,
	courseGrades: {
		"Intro to Programming": 4,
		"JavaScript Basics": 3,
		"Functional Programming": 5
	},
}


const courseName = "JavaScript Basics";
console.log(
	`${student.name} got a ${student.courseGrades[courseName]}`
	+ `from ${courseName}`);
console.log(student);