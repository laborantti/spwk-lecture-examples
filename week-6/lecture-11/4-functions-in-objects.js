
const resetName = function() { // DO NOT USE ARROW FUNCTION HERE.
	this.name = "DEFAULT NAME";
};

const student = {
	name: "Aili",
	credits: 45,
	courses: [
		{
			name: "JavaScript Basics",
			grade: 3,
		},
		{
			name: "Intro to Programming",
			grade: 2,
		},
		{
			name: "Functional Programming",
			grade: 5,
		},
	],
	resetName: resetName,
};

// console.log(this);
student.resetName();
console.log(student);

function addCourse(courseName, courseGrade) {
	const course = {
		name: courseName,
		grade: courseGrade,
	};
	this.courses.push(course);
}

// addCourse("Käsienheiluttelun alkeet", 1);

student.addCourse = addCourse;
student.addCourse("Web-kehityksen starttipaketti", 5);

console.log(student);
/* how console.log works in principle
const console = {
	log: function(str) {
		// prints str to console;
	},
}
*/