const obj = {
	avain1: "arvo1",
	hörönplöö: 25,
	avain3: true,
	avain4: undefined,
	other_obj: {
		x: 14,
		y: 1902341,
	},
}
console.log(obj.hörönplöö);
console.log(obj.avain2);
console.log(obj.other_obj.x);

const playerCharacter = {
	position: {
		x: 123,
		y: 12342,
	},
	name: "Pelaaja",
	canJump: true,
}
playerCharacter.position.x += 1;
playerCharacter.name = "Matti";
playerCharacter["name"] = "Matti";
	// does the same as the row above

const thingWeWant = "position";

console.log(playerCharacter[thingWeWant]);

// can't be done because it's const:
// playerCharacter = { name: "jokumuu" }