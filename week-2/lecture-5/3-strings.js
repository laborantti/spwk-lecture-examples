const name1 = "Matti";
const name2 = "Teppo";
const number1 = 1234;

console.log(name1);
console.log(name1[-1]);
console.log(name1[0]);
console.log(name1[1]);
console.log(name1[2]);
console.log(name1[3]);
console.log(name1[4]);
console.log(name1[5]);
console.log(name1.length);
console.log(name1[name1.length - 1]);
console.log("matti\nja\nteppo\\n");
console.log(`Bändin nimi on ${name1} ja ${name2}`); // string literal
console.log(`PIN-koodi on ${number1}`); 			// string literal
console.log(`PIN-koodi on ${number1}`); 			// string literal
console.log("PIN-koodi on", number1);
