const shapes = [
	"square",
	"dodecahedron",
	"circle",
	"squircle",
	"triangle",
	234,
	1,
	1203912903,
	true,
	"voi ei",
	undefined,
];
console.log(shapes.length);
shapes.pop();
shapes.pop();
shapes.pop();
shapes.pop();
shapes.pop();
shapes.pop();
shapes.push("hexagon");
console.log(shapes.length);

// shapes[shapes.length - 1] = "pentagon";
// ei toimi:
// shapes = ["kolmio", "viisikulmio", "niin pois päin"];
// shapes = "hehehehe";

console.log(shapes[0]); // first element
console.log(shapes[0][0]); // first element's first letter
console.log(shapes[shapes.length - 1]); // last element
console.log(shapes.toString());
console.log(typeof(shapes));