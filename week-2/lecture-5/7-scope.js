{

	let number1 = 123;

	{
		const esim = "esimerkki 1";
			// const/let only usable inside a block scope
		number1 = 567;
		console.log(number1);
		var toinenEsim = "hehee"; // var is in global scope!!!
	}
	{
		const esim = "esimerkki 2";
	}
	console.log(toinenEsim);
	console.log(`voi juku ${toinenEsim}`);
}