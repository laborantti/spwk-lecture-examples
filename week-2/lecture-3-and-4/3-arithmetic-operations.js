let a = 10;
const b = 5;

// console.log("Sum: " + (a + b)); // "Sum: 10" + b
// console.log("Difference: " + (a - b));
// console.log("Product: " + a * b);
// console.log("Fraction: " + a / b);

// console.log("Sum:", a + b);
// console.log("Difference:", a - b);
// console.log("Product:", a * b);
// Ei näin: console.log("Fraction:",a/b);
console.log(a, "+", b, "=", a + b);
console.log(a, "-", b, "=", a - b);
console.log(a, "*", b, "=", a * b);
console.log(a, "/", b, "=", a / b);

// MODULO / MODULUS / REMAINDER
console.log(a, "%", b, "=", a % b);
a++;
console.log(a, "%", b, "=", a % b);
a++;
console.log(a, "%", b, "=", a % b);
a++;
console.log(a, "%", b, "=", a % b);
a++;
console.log(a, "%", b, "=", a % b);
a++;
console.log(a, "%", b, "=", a % b);





let sumOfCoffeeCups = 0;

const coffeeCupsPerttu = 8;
const coffeeCupsJarkko = 3;
const coffeeCupsHeli = 6;
const coffeeCupsMatti = 6;
const coffeeCupsTeppo = 6;

sumOfCoffeeCups += coffeeCupsPerttu;
// SAME AS: 
// 	sumOfCoffeeCups = sumOfCoffeeCups + coffeeCupsPerttu;
sumOfCoffeeCups -= coffeeCupsPerttu;
// SAME AS: 
// 	sumOfCoffeeCups = sumOfCoffeeCups - coffeeCupsPerttu;

sumOfCoffeeCups++;
// SAME AS: sumOfCoffeeCups += 1;
// SAME AS: sumOfCoffeeCups = sumOfCoffeeCups + 1;

sumOfCoffeeCups--;
// SAME AS: sumOfCoffeeCups -= 1;
// SAME AS: sumOfCoffeeCups = sumOfCoffeeCups - 1;


sumOfCoffeeCups = sumOfCoffeeCups + coffeeCupsJarkko;
sumOfCoffeeCups = sumOfCoffeeCups + coffeeCupsHeli;
