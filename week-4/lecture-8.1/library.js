
// this code cannot be executed

const bookName = "Sota ja Rauha";

/**
 * Tries to go given library
 * @param {string} libraryName name of library we want to go to
 * @returns boolean based on success
 */
function goToTheLibrary(libraryName)
{
	const address = findAddressOfLibrary(libraryName);
	// things happen
	const didWeGetToTheLibrary = true;
	return didWeGetToTheLibrary;
}

function findTheBookFromTerminal(bookName)
{
	// things happen
	return shelfCode; 
}

if (reserveBook(bookName))
{
	if (goToTheLibrary())
	{
		const shelfCode = findTheBookFromTerminal(bookName);
		const bookId = pickUpBookFromShelf(shelfCode);
		if (!scanBook(bookId))
			getHelp();
	}
	goHome();
}