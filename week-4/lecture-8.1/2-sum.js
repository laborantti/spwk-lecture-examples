function sum(number1, number2)
{
	const sumOfNumbers = number1 + number2;
	console.log(`${number1} + ${number2} = ${sumOfNumbers}`);
	return sumOfNumbers;
}

console.log(typeof(sum(1,2)));
console.log(typeof(sum));