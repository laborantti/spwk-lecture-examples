let myNumber = 11;

/**
 * Does stuff to number
 * @param {number} number1 the number that we want to do stuff to 
 * @returns the number that we did stuff to
 */
function doStuffToNumber(number1)
{
	number1 = 110;
	return number1;
}

console.log(myNumber);
myNumber = doStuffToNumber(myNumber);
console.log(myNumber);
