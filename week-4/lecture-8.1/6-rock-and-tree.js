const tree = { name: "tree", x: 6, y: 7, hitpoints: 30 };
const rock = { name: "rock", x: 3, y : 11, hitpoints: 90 };
const rock2 = { name: "rock2", x: 3, y : 11, hitpoints: 900 };

// primitive parameter version
rock.hitpoints = Damage2(rock.hitpoints, rock.name, 15);

// reference parameter version
Damage(rock, 10);
Damage(rock, 5);
Damage(rock2, 150);

Damage(tree, 20);
Damage(tree, 1);

function Damage(target, damage)
{
	target.hitpoints -= damage;
	console.log(`${target.name} hitpoints left: ${target.hitpoints}`);
}

function Damage2(hitpoints, name, damage)
{
	hitpoints -= damage;
	return hitpoints;
}