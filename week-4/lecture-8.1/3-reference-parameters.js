const array1 = [11, 22, 33];

/**
 * Function that does stuff to array
 * @param {*} arr 
 */
function doStuffToArray(arr)
{
	arr[0] = 110;
	arr = [234234, 0, 1231, 2];
}

console.log(array1);
doStuffToArray(array1);
console.log(array1);
