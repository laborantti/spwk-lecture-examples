function exercise2()
{
	console.log("oikea vastaus!");
}

function exercise7()
{
	function factorial(number) {
		let numberFactorial = 1;
		for (i=1; i<= number; i++) {
			numberFactorial *= i;
		}
		return numberFactorial;
	}
	
	let n = 1;
	let nFactorial = 0;
	while (true) {
		nFactorial = factorial(n);
		if (nFactorial % 600 == 0) {
			break;
		}
		n++;
	}
	console.log(n, nFactorial);
}

exercise2();
exercise7();