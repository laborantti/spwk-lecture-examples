let i = 1;
let text = "";
const limit = 5;

while (i <= limit)
{
	const delimiter = (i === limit) ? "" : ", ";
	text += i * 3 + delimiter;
	i++;
}
console.log(text);