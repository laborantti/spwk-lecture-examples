const answerIsCorrect = false;
const positiveFeedback = "Very nice!";
const negativeFeedback = "Keep practising";

let feedbackText = "";
if (answerIsCorrect)
{
	feedbackText = positiveFeedback;
} else {
	feedbackText = negativeFeedback;
}

const feedbackTextTernary =
	answerIsCorrect ? positiveFeedback : negativeFeedback;

console.log(feedbackTextTernary);
