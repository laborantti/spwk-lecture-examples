
const a = 3;
const b = 1;

if (a > b) {
	console.log("A on suurempi kuin B!");
}

if (true) { // a > b
	console.log("tosi on");
}

const isBGreaterThanA = b > a; // false
if (isBGreaterThanA) {
	console.log("B on suurempi kuin A!");
}