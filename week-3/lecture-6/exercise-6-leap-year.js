const year = 1701;

const yearIsDivisibleByFour = year % 4 === 0;
const yearIsACentury = year % 100 === 0;
const yearIsDivisibleBy400 = year % 400 === 0;

if (yearIsDivisibleByFour) {
	if (yearIsACentury) {
		if (yearIsDivisibleBy400) {
			console.log("it's a leap year!");
		} else {
			console.log("it's not a leap year!");
		}
	} else {
		console.log("it's a leap year!");
	}
} else {
	console.log("it's not a leap year!");
}

const leapYear = "it's a leap year!";
const notLeapYear = "it's not a leap year!";

const result = yearIsDivisibleByFour ? 
	(yearIsACentury ?
		(yearIsDivisibleBy400 ?
			leapYear
			: notLeapYear
		) : leapYear
	) : notLeapYear

console.log(result);



