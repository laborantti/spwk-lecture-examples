const hasCheese = true;
const hasTomato = true;
const hasDough = true;
const hasPineapple = false;

if (hasCheese && hasTomato && hasDough) {
	if (hasPineapple) {
		console.log("it is a divisive pizza");
	} else {
		console.log("it is a basic pizza!");
	}
} else {
	console.log("it is not a pizza");
}