const itIsRaining = false;
const itIsSnowing = false;
const sunIsShining = true;
const rainbowTime = itIsRaining && sunIsShining;

if (rainbowTime) // AND
{
	console.log("taitaa olla kesä");
}
if (itIsRaining || itIsSnowing) // OR
{
	console.log("asioita tippuu taivaalta");
}

// (itIsRaining) on sama kuin (itIsRaining === true)
if (!itIsRaining === true) { // sama kuin: itIsRaining === false
	console.log("no rain detected");
}
if (itIsRaining === true) {
	console.log("rain detected!");
}

if (!(itIsRaining || itIsSnowing))
{
	console.log("nothing falls from the sky");
}
if (!itIsRaining && !itIsSnowing)
{
	console.log("nothing falls from the sky");
}

