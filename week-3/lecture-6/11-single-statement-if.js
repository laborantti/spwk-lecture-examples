let numberOfLeapYears = 0;


const year = 1700;

const isItALeapYear = (year % 4 === 0 && 
	((year % 400 === 0) || (year % 100 !== 0))
);

if (isItALeapYear)
{
	console.log("it is a leap year.");
	numberOfLeapYears += 1;
}

console.log(numberOfLeapYears);
