const itIsRaining = true;
const sunIsShining = true;

if (itIsRaining)
{
	if (sunIsShining) {
		console.log("we can see a rainbow!");
	} else {
		console.log("it is raining! let's stay inside");
	}
} else {
	if (sunIsShining) {
		console.log("Sun is shining! let's go outside");
	} else {
		console.log("no sun, no rain");
	}
}

