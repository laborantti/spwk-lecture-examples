const year = 1700;

const isItALeapYear = (year % 4 === 0 && 
	((year % 400 === 0) || (year % 100 !== 0))
);

const result = isItALeapYear ?
	"It is a leap year." : "It is not a leap year.";

console.log(result);