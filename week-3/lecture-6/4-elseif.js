const a = 20;
const b = 20;


if (a > b) {
	console.log("a on suurempi kuin b");
} else if (a < b) {
	console.log("b on suurempi kuin a!");
} else {
	console.log("a ja b ovat yhtä suuret!");
}

const name1 = "matti";
const name2 = "teppo";

if (name1 === name2) {
	console.log("samat nimet");
} else {
	console.log("eri nimet!");
}
