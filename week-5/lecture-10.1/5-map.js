const fruits = ["banana", "apple", "pear", "pineapple", "lemon", "🍊"];
const fruitLengthsOver5 = fruits
	.map(fruit => fruit.length)
	.filter(len => len > 5);
console.log(fruitLengthsOver5);

fruitfruits = fruits.map(fruit => fruit + " " + fruit);
console.log(fruitfruits);

const numbers = [3, 56, 4, 8, 33];
const incrementedNumbers = numbers
	.map(number => ++number)
	.map(number => number**2)
	.map(number => number - 30)
	.map(number => Math.abs(number));

console.log(incrementedNumbers);