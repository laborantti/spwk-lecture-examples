const fruits = ["banana", "apple", "pear", "pineapple", "lemon", "tomato"];
// fruitName + "belongs in a pizza";
// fruitName + "does not belong in a pizza";
const fruitArray = fruits.map(fruit => {
	if (fruit === "pineapple" || fruit === "tomato") {
		return fruit + " belongs in a pizza";
	} else {
		return fruit + " does not belong in a pizza";
	}
});
console.log(fruitArray);