const fruits = ["banana", "apple", "pear", "pineapple", "lemon", "🍊"];
let longFruit;
for (fruit of fruits) {
	if (fruit.length > 5)
	{
		longFruit = fruit;
		break;
	}
}
const longFruit2 = fruits.find(function(fruit) {
	if (fruit.length < 5) {
		return true;
	} else {
		return false;
	}
});
const longFruit3 = fruits.find(function(fruit) {
	return fruit.length < 5;
});
const longFruit4 = fruits.find((fruit) => {
	return fruit.length < 5;
});
const longFruit5 = fruits.find(fruit => fruit.length < 5);
console.log("found fruit", longFruit5);
