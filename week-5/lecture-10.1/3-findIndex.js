const fruits = ["banana", "apple", "pear", "pineapple", "lemon", "🍊"];
const longFruitIndex = fruits.findIndex(fruit => fruit.length > 6);

console.log("found fruit", fruits[longFruitIndex]);

fruits.splice(longFruitIndex);

console.log(fruits);