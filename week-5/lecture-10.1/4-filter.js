const fruits = ["banana", "apple", "pear", "pineapple", "lemon", "🍊"];

const longFruits = fruits.filter((fruit, index) => 
	(fruit.endsWith("e") && index > 2 ));

const longFruits2 = fruits
	.filter((f, index) => index > 2)
	.filter(fruit => fruit.endsWith("e"));

console.log("found fruits", longFruits2);
// console.log(fruits);