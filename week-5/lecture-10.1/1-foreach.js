const ages = [13, 21, 33, 17, 46];
let sum = 0;

const whatIsIt = ages.forEach(number => sum += number);
console.log(whatIsIt); // undefined

for (number of ages) // for..of version
{
	sum += number;
}

ages.forEach((number, index) => 
	console.log(`${index}th element of array is ${number}`)
);
console.log(sum);