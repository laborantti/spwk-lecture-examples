const numbers = [5, 7, 2, 9, 3, 13, 15, 6, 17, 24];

for (const i in numbers) {
  if(numbers[i] % 3 === 0) {
    console.log(numbers[i]);
  }
}

console.log("\n");

const numbers2 = [5, 7, 2, 9, 3, 13, 15, 6, 17, 24];

for (const number of numbers2) {
  if(number % 3 === 0) {
    console.log(number);
  }
}
