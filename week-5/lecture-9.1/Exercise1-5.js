// Exercise 1

const array = ["the", "quick", "brown","fox"];

console.log(array);
console.log(array[1]);
console.log(array[2]);

// Exercise 2

array[2] = "gray";
console.log(array);

// Exercise 3

array.push("over");
array.push("lazy");
array.push("dog");

console.log(array);

array.unshift("pangram:"); // Add to the beginning of the array

console.log(array);

// Exercise 4

array.splice(5, 0, "jump"); // Starting from element 5, delete nothing, add "jump"
array.splice(7, 0, "the"); // Starting from element 7, delete nothing, add "the"

console.log(array);

// Exercise 5

array.shift(); // Remove first element

console.log(array);

array.splice(4); // Remove everything starting from element 4

console.log(array);

array.splice(2, 1); // Starting from element 2, remove 1 element

console.log(array);
